import React from 'react';
import PropTypes from 'prop-types';

const EmojiCount = (props) => {
  return (
    <div className="emojiCount">
      Questions <span>{props.counter}</span> of <span>{props.total}</span>
    </div>
  );
}

EmojiCount.propTypes = {
  counter: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired
};

export default EmojiCount;