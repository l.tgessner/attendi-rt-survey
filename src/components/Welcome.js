import React from 'react';
import '../App.css';

const WelcomeComp = () => {

    return (
        <React.Fragment>
        <h1>WELCOME</h1>
            <p style={{marginTop:"20px auto", display:"block"}}>Take part in this survey!</p>
                <button style={{marginTop:"50px auto"}}>Start</button>
        </React.Fragment>
    );
}

export default WelcomeComp;