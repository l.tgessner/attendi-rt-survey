import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

import WelcomeComp from "./components/Welcome";
import Questions from './components/Questions';
import EmojiOptions from './components/EmojiOptions';
import EmojiCount from './components/EmojiCount';
import ScoreEnd from './components/ScoreEnd';


class App extends Component {

	startSurvey = (e) => {
		//prevent from reloading after button pressed
		e.preventDefault();
		axios.post('https://tkfn9ciovh.execute-api.us-west-1.amazonaws.com/qa/api/surveys/synchronization')

		// converting to json format
		.then(res => res)
		.then(json => {
			this.setState({
				isLoaded: true,
				items: json,
				// to get and save data inside the app component
			})
		})
		.catch(err => console.error('Error:', err));
	}

	render() {
		return (
			<div className="App">
				<h2>Attendi RT Survey</h2>
					<WelcomeComp/>
					<Questions content="Esto es una pregunta prueba" />
			</div>
		);
	}
}


export default App;